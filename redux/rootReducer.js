import { combineReducers } from 'redux';
import homeReducer from '@/pages/home/reducer';
import gameReducer from '@/pages/game/reducer';

const rootReducer = combineReducers({
  profile: homeReducer,
  game: gameReducer,
});

export default rootReducer;
