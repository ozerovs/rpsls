import { SET_ROUND, RESET_GAME } from './types';
import axios from 'axios';
import { getComputerMove, getUserRoundStatus } from '@/pages/game/utils';

export const setRound = (payload) => async (dispatch) => {
  await axios.get('api/randomNumber')
    .then(res => {
      const computer = getComputerMove(res.data.random_number);
      const user = payload;

      dispatch({
        type: SET_ROUND,
        payload: {
          user,
          computer,
          winner: getUserRoundStatus({ user, computer }),
        },
      });
    })
    .catch(err => {
      console.log(err, 'here is err');
    });

};

export const resetGame = () => (dispatch) => {
  dispatch({
    type: RESET_GAME,
  });
};
