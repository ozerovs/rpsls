export const CONTROLS = ['🤘', '🦎', '🖖', '✂️', '🧻'];

export const DRAW_ID = 'tie';
export const USER_WIN_ID = 'win';
export const USER_LOST_ID = 'lose';

export const MAX_DISPLAY_SIZE = 10;
