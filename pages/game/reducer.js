import { RESET_GAME, SET_ROUND } from './types';
import { getUpdatedStatistics } from './utils';

const getDefaultState = () => ({
  rounds: [],
  currentStatistics: {
    user: 0,
    computer: 0,
  },
  completedGames: [],
});

const gameReducer = (state = getDefaultState(), action) => {
  switch(action.type) {
  case SET_ROUND:
    const { winner, ...roundsData } = action.payload;
    const rounds = [...state.rounds, roundsData];
    const currentStatistics = getUpdatedStatistics({
      currentStatistics: state.currentStatistics,
      winner,
    });

    return {
      ...state,
      rounds,
      currentStatistics,
    };
  case RESET_GAME:
    return {
      ...state,
      completedGames: [...state.completedGames, state.currentStatistics],
      rounds: [],
      currentStatistics: {
        user: 0,
        computer: 0,
      },
    };
  default:
    return { ...state };
  }
};

export default gameReducer;
