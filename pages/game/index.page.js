import { useState } from 'react';
import { useDispatch } from 'react-redux';
import clsx from 'clsx';
import Layout from '@/components/layout/game';
import styles from './index.module.scss';
import Controls from './components/controls';
import { useCompletedGames, useRounds, useUsername } from './hooks';
import { Rules } from '@/pages/game/components/rules';
import Moves from '@/pages/game/components/moves';
import Button from '@/components/ui/button';
import { resetGame } from '@/pages/game/actions';

const Game = () => {
  const [isAsideActive, setIsAsideActive] = useState();
  const username = useUsername();
  const rounds = useRounds();
  const completedGames = useCompletedGames();
  const dispatch = useDispatch();

  const greetingString = `${username} vs Computer`;

  const handleRestart = () => {
    dispatch(resetGame());
  };

  const handleMobileMenu = () => {
    setIsAsideActive((prevAsideActive) => !prevAsideActive);
  };

  const renderCompletedGames = (game, index) => {
    const resultString = `${username}: ${game.user}, Computer: ${game.computer}`;

    return (
      <li key={index + resultString}>
        {resultString}
      </li>
    );
  };

  const asideClassName = clsx(styles.aside, {
    [styles.asideActive]: isAsideActive,
  });

  return (
    <Layout>
      <div className={styles.root}>
        <main className={styles.main}>
          <div className={styles.game}>
            <h1>{greetingString}</h1>
            {rounds.length ? <Moves /> : <Rules />}
          </div>
          <footer className={styles.footer}>
            <div className={styles.description}>
              <span>Select your move</span>
              <Button
                className={styles.button}
                disabled={!rounds.length}
                onClick={handleRestart}
              >
                Reset game
              </Button>
            </div>
            <Controls />
          </footer>
        </main>
        <aside className={asideClassName}>
          <span className={styles.mobileMenu} onClick={handleMobileMenu}>
            <span className={styles.burger}></span>
          </span>
          <h2>Game history</h2>
          <ol className={styles.completedList}>
            {completedGames.map(renderCompletedGames)}
          </ol>
        </aside>
      </div>
    </Layout>
  );
};

export default Game;
