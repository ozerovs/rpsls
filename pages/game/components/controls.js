import styles from './controls.module.scss';
import { useDispatch } from 'react-redux';
import { setRound } from '@/pages/game/actions';
import { CONTROLS } from '@/pages/game/constants';

const Controls = () => {
  const dispatch = useDispatch();

  const renderControl = (controlValue, index) => {
    const handleControlSelect = () => {
      dispatch(setRound(index));
    };

    return (
      <li className={styles.control} key={controlValue} onClick={handleControlSelect}>
        {controlValue}
      </li>
    );
  };

  return (
    <ul className={styles.controlsRow}>
      {CONTROLS.map(renderControl)}
    </ul>
  );
};

export default Controls;
