import clsx from 'clsx';
import { useRounds, useUsername } from '@/pages/game/hooks';
import { getSlicedRounds, getUserRoundStatus, getWinner } from '@/pages/game/utils';
import { CONTROLS, USER_LOST_ID, USER_WIN_ID } from '@/pages/game/constants';
import styles from './moves.module.scss';


const Moves = () => {
  const rounds = useRounds();
  const username = useUsername();

  const renderMove = (data, index) => {
    const { user, computer } = data;
    const result = getUserRoundStatus(data);

    const winner = getWinner({ username, result });

    const winnerClassName = clsx({
      [styles.userWin]: result === USER_WIN_ID,
      [styles.computerWin]: result === USER_LOST_ID,
    });

    return (
      <li className={styles.listItem} key={index + JSON.stringify(data)}>
        <span className={styles.user}>{CONTROLS[user]}</span>
         vs
        <span className={styles.computer}>{CONTROLS[computer]}</span>
        <span className={winnerClassName}>{winner}</span>
      </li>
    );
  };

  return (
    <ol className={styles.list}>
      {getSlicedRounds(rounds).map(renderMove)}
    </ol>
  );
};

export default Moves;
