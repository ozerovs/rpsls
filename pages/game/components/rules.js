import Image from 'next/image';
import image from '../images/img.png';
import styles from './rules.module.scss';

export const Rules = () => (
  <div>
    <p>Rules are simple - you compete vs computer by selecting your move from the list below. Use image as a reference to what beats what.</p>
    <p>Start by selecting any move from the list!</p>
    <span className={styles.imageContainer}>
      <Image src={image} />
    </span>
  </div>
);
