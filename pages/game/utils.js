import {
  CONTROLS,
  DRAW_ID,
  MAX_DISPLAY_SIZE,
  USER_LOST_ID,
  USER_WIN_ID,
} from './constants';

export const getComputerMove = (number) => {
  return number % CONTROLS.length;
};

// This approach supports more complex versions of game with more "moves".
// Only requirement - odd amount of moves
const getUserWinnableIds = (id) => {
  const result = [];
  for (let i = 1; i < CONTROLS.length; i += 2) {
    result.push(id + i);
  }

  return result.reduce((acc, elem, index) => {
    if (elem >= CONTROLS.length) {
      acc[index] = elem - CONTROLS.length;
    }

    return acc;
  }, result);
};

export const getUserRoundStatus = ({ user, computer }) => {
  if (user === computer) return DRAW_ID;

  if (getUserWinnableIds(user).includes(computer)) {
    return USER_WIN_ID;
  }

  return USER_LOST_ID;
};

export const getWinner = ({ username, result }) => {
  if (result === DRAW_ID) return 'Draw!';
  if (result === USER_WIN_ID) return `${username} won!`;
  return 'Computer won!';
};

export const getSlicedRounds = (rounds) => {
  if (rounds.length < MAX_DISPLAY_SIZE) return rounds;

  return rounds.slice(rounds.length - MAX_DISPLAY_SIZE);
};

export const getUpdatedStatistics = ({ currentStatistics, winner }) => {
  const updatedStatistics = { ...currentStatistics };

  if (winner === USER_WIN_ID) {
    updatedStatistics.user += 1;
  } else if (winner === USER_LOST_ID) {
    updatedStatistics.computer += 1;
  }

  return updatedStatistics;
};
