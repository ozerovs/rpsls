import { useSelector } from 'react-redux';

const getUsername = ({ profile: { username } }) => username;
export const useUsername = () => useSelector(getUsername);

const getRounds = ({ game: { rounds } }) => rounds;
export const useRounds = () => useSelector(getRounds);

const getCompletedGames = ({ game: { completedGames } }) => completedGames;
export const useCompletedGames = () => useSelector(getCompletedGames);
