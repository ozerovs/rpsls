import { useState } from 'react';
import { useDispatch } from 'react-redux';
import { useRouter } from 'next/router';
import Layout from '@/components/layout/landing';
import Input from '@/components/ui/input';
import Button from '@/components/ui/button';
import { setUsername } from './actions';
import styles from './index.module.scss';

export default function Home() {
  const [userName, setUserName] = useState('');
  const dispatch = useDispatch();
  const router = useRouter();

  const handleInput = (event) => {
    setUserName(event.target.value);
  };

  const handleButtonClick = () => {
    dispatch(setUsername(userName));
    router.push('/game');
  };

  return (
    <Layout>
      <article className={styles.container}>
        <Input label="Enter name of your character" onInput={handleInput} />
        <Button disabled={!userName} onClick={handleButtonClick}>Enter</Button>
      </article>
    </Layout>
  );
}
