import { SET_USERNAME } from './types';

const getDefaultState = () => ({
  username: 'You',
});

const homeReducer = (state = getDefaultState(), action) => {
  switch(action.type) {
  case SET_USERNAME:
    return {
      ...state,
      username: action.payload,
    };
  default:
    return { ...state };
  }
};

export default homeReducer;
