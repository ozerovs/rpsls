import { SET_USERNAME } from './types';

export const setUsername = (payload) => (dispatch) => {
  dispatch({
    type: SET_USERNAME,
    payload,
  });
};
