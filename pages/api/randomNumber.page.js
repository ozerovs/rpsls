import axios from 'axios';
// Thought that this is fun to use!
const URL = 'https://codechallenge.boohma.com/random';

const handler = (req, res) => {
  return axios.get(URL)
    .then((response) => {
      return res.status(200).json(response.data);
    })
    .catch((error) => {
      return res.status(error.code).json({ error });
    });
};

export default handler;
