import Head from 'next/head';
import styles from './index.module.scss';

const LandingLayout = ({ children }) => (
  <>
    <Head>
      <title>RPSLS game</title>
      <meta name="description" content="Rock-Paper-Scissors-Lizard-Spock" />
      <link rel="icon" href="/favicon.ico" />
    </Head>
    <main className={styles.main}>
      {children}
    </main>
    <footer className={styles.footer}>
      developed by Sergei Ozerov
    </footer>
  </>
);

export default LandingLayout;
