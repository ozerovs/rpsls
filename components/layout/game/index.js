import Head from 'next/head';

const GameLayout = ({ children }) => (
  <>
    <Head>
      <title>RPSLS game</title>
      <meta name="description" content="Rock-Paper-Scissors-Lizard-Spock" />
      <link rel="icon" href="/favicon.ico" />
    </Head>
    {children}
  </>
);

export default GameLayout;
