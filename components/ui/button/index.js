import styles from './index.module.scss';

const Button = ({ children, ...cleanProps }) => (
  <button className={styles.root} {...cleanProps}>{children}</button>
);

export default Button;
