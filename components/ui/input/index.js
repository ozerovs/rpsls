import PropTypes from 'prop-types';
import styles from './index.module.scss';

export const Input = ({ label, ...cleanProps }) => (
  <label>
    {label}
    <input className={styles.input} type="text" {...cleanProps} />
  </label>
);

Input.propTypes = {
  label: PropTypes.string,
};

export default Input;
