This is a Rock-Paper-Scissors-Lizard-Spock project, based on [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Running locally

For the development server:

```bash
npm run dev
# or
yarn dev
```

## Running with Docker

If you want to run with Docker - there's a Dockerfile included in the root. 
Use following commands to start it up

- `docker build -t rpsls .`
- `docker run -dp 3000:3000 rpsls`

##
